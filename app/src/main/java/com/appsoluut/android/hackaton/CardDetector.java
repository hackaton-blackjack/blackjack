package com.appsoluut.android.hackaton;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by p.hameteman on 12/03/2017.
 */

public class CardDetector {
    // Lower and Upper bounds for range checking in HSV color space
    private Scalar lowerBound = new Scalar(0);
    private Scalar upperBound = new Scalar(0);

    // Minimum contour area in percent for contours filtering
    private static double minContourArea = 0.1;

    // Cache
    Mat pyrDownMat = new Mat();
    Mat hsvMat = new Mat();
    Mat mask = new Mat();
    Mat dilatedMask = new Mat();
    Mat hierarchy = new Mat();

    private List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

    public void process(Mat rgbaImage) {
        Imgproc.pyrDown(rgbaImage, pyrDownMat);
        Imgproc.pyrDown(pyrDownMat, pyrDownMat);

        Imgproc.cvtColor(pyrDownMat, hsvMat, Imgproc.COLOR_RGB2HSV_FULL);

        Core.inRange(hsvMat, lowerBound, upperBound, mask);
        Imgproc.dilate(mask, dilatedMask, new Mat());

        List<MatOfPoint> contours = new ArrayList<>();

        Imgproc.findContours(dilatedMask, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        // Find max contour area
        double maxArea = 0;
        Iterator<MatOfPoint> each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }

        // Filter contours by area and resize to fit the original image size
        this.contours.clear();
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > minContourArea *maxArea) {
                Core.multiply(contour, new Scalar(4, 4), contour);
                this.contours.add(contour);
            }
        }
    }

    public List<MatOfPoint> getContours() {
        return contours;
    }

}
